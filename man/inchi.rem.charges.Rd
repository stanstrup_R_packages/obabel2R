\name{inchi.rem.charges}
\alias{inchi.rem.charges}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Remove charges from a molecule InChI
}
\description{
Remove charges from a molecule InChI.
Calls obabel locally. Hence obabel need to be available in path.
}
\usage{
inchi.rem.charges(inchi,verbose=F)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{inchi}{Character vector InChIs.}
  \item{verbose}{Print additional information from obabel.}
}

\value{A character vector of InChIs.}

\author{
Jan Stanstrup, \email{stanstrup@gmail.com}
}
